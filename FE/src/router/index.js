import Vue from 'vue'
import VueRouter from 'vue-router'
import Dashboard from '@Pages/dashboard.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Dashboard',
    component: Dashboard
  },
  {
    path: '/book',
    name: 'Book',
    component: () => import(/* webpackChunkName: "settings" */ '@Pages/book.vue')
  },
  {
    path: '/patron',
    name: 'Patron',
    component: () => import(/* webpackChunkName: "settings" */ '@Pages/patron.vue')
  },
  {
    path: '/settings',
    name: 'Settings',
    component: () => import(/* webpackChunkName: "settings" */ '@Pages/settings.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
