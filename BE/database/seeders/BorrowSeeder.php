<?php

namespace Database\Seeders;

use App\Models\BorrowedBook;
use Illuminate\Database\Seeder;

class BorrowSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach(range(1,10) as $index){
            BorrowedBook::create([
                'book_id' => rand(1,10),
                'copies' => rand(1,15),
                'patron_id' => rand(1,10)
            ]);
        }
    }
}