<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BookController;
use App\Http\Controllers\PatronController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\BorrowBookController;
use App\Http\Controllers\ReturnBookController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::apiResources([
  '/books' => BookController::class,
  '/patrons' => PatronController::class, 
  '/categories' => CategoryController::class,
  '/borrowedbook' => BorrowBookController::class,
  '/returnedbook' => ReturnBookController::class
 ]);