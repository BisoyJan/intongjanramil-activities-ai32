<?php

namespace App\Http\Controllers;

use App\Models\Patron;
use Illuminate\Http\Request;
use App\Http\Requests\PatronRequest;

class PatronController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(Patron::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PatronRequest $request)
    {
        $Patrons = new Patron();

        $Patrons->last_name = $request->last_name;
        $Patrons->first_name = $request->first_name;
        $Patrons->middle_name = $request->middle_name;
        $Patrons->email = $request->email;

        $validated = $request->validated();
        $Patrons->save();
        return response()->json($Patrons);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $Patrons = Patron::find($id);
        return response()->json($Patrons);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PatronRequest $request, $id)
    {
        $Patrons = Patron::where('id', $id)->firstOrFail();
        $Patrons->update($request->validated());
        return response()->json(['message' => 'Patron updated.', 'patron' => $Patrons]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $patron = Patron::where('id', $id)->firstOrFail();
            $patron->delete();
            
            return response()->json(['message' => 'Patron deleted.']);
        } catch (ModelNotFoundException $e) {
            return response()->json(['message' => 'Patron not found.'], 404);

        }
    }
}
