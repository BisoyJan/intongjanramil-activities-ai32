var ctx = document.getElementById('bar').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ['JAN', 'FEB', 'MARCH', 'APRIL', 'MAY', 'JUNE'],
        datasets: [{
            label: 'Borrowed Books, Returned Books',
            data: [6, 12, 8, 5, 2, 5],
            backgroundColor: [
                'rgba(192,57,43 ,1)',
                'rgba(41,128,185 ,1)',
                'rgba(192,57,43 ,1)',
                'rgba(41,128,185 ,1)',
                'rgba(192,57,43 ,1)',
                'rgba(41,128,185 ,1)'
            ],
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});


var pie = document.getElementById('pie').getContext('2d');
var myChart = new Chart(pie, {
    type: 'pie',
    data: {
        labels: ['Novel', 'Fiction', 'Biography', 'Horror'],
        datasets: [{
            label: '# of Votes',
            data: [10, 8, 7, 5],
            backgroundColor: [
                'rgba(26,188,156 ,1)',
                'rgba(41,128,185 ,1)',
                'rgba(192, 57, 43,1.0)',
                'rgba(241,196,15 ,1)'
                
            ],
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});



